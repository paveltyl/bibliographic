package User;

public class User {
    public String name;
    public String login;
    public String password;
    public int id;
    public Role role;

    public User() {
        name = "";
        login = "";
        password = "";
        role = Role.AUTHOR;
        id = -1;
    }
}
