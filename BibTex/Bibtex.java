package BibTex;

import org.jbibtex.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by pashatyl on 24/5/17.
 */
public class Bibtex {
    private String dbPath = "./assets/bibtex.bib";
    private BibTeXDatabase database = null;
    private static Bibtex instance = null;

    private Bibtex() {
        Reader reader = null;
        try {
            reader = new FileReader(dbPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        try {
            BibTeXParser bibtexParser = new BibTeXParser();
            database = bibtexParser.parse(reader);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static Bibtex getInstance() {
        if (instance == null) {
            instance = new Bibtex();
        }
        return instance;
    }

    public void parseFile(String filename) throws FileNotFoundException, ParseException {
        Reader reader = new FileReader(filename);
        BibTeXParser bibtexParser = new BibTeXParser();
        BibTeXDatabase db = bibtexParser.parse(reader);

        Map<Key, BibTeXEntry> entryMap = db.getEntries();
        Collection<BibTeXEntry> entries = entryMap.values();
        for (org.jbibtex.BibTeXEntry entry : entries) {
            database.addObject(entry);
        }
    }

    public ArrayList<Publication> getAuthorsPublications(String name) {
        return getPublications((str) -> {
            return str.equals(name);
        });
    }

    public ArrayList<Publication> getAllPublications() {
        return getPublications((str) -> {
            return true;
        });
    }

    public ArrayList<Publication> getPublications(Function<String, Boolean> fn) {
        Map<org.jbibtex.Key, org.jbibtex.BibTeXEntry> entryMap = database.getEntries();
        ArrayList<Publication> correctEntries = new ArrayList<>();
        Collection<org.jbibtex.BibTeXEntry> entries = entryMap.values();
        for (org.jbibtex.BibTeXEntry entry : entries) {
            org.jbibtex.Value value = entry.getField(BibTeXEntry.KEY_AUTHOR);
            if (fn.apply(value.toUserString())) {
                correctEntries.add(new Publication(entry));
            }
        }
        return correctEntries;
    }

    public void deleteEntries(Publication[] toDelete){
        for (Publication e : toDelete) {
            database.removeObject(e.entry);
        }
        updateDB();
    }

    public void updateDB() {
        try {
            Writer writer = new FileWriter(dbPath);
            BibTeXFormatter bibtexFormatter = new BibTeXFormatter();
            bibtexFormatter.format(database, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        updateDB();
    }
}
