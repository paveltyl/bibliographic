import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import BibTex.Bibtex;
import BibTex.Publication;
import FormsManager.FormsManager;
import User.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Ann on 23.05.2017.
 */
public class AuthorForm extends JFrame {

    private final int WIDTH = 1000;
    private final int HEIGHT = 300;

    private User user;
    private Bibtex bibtex = Bibtex.getInstance();

    private JPanel authorPanel;
    private JLabel authorLabel;
    private JButton uploadBibFileBut;
    private JTable publicationsTable;
    private JButton indexBut;

    public AuthorForm(User author) {
        super();
        this.user = author;

        setSize(WIDTH, HEIGHT);
        setGUIElements();
        setListeners();
    }

    private void setGUIElements() {
        setTitle("bib-system/author");

        setContentPane(authorPanel);
        authorPanel.setVisible(true);

        authorLabel.setText(user.name);

        updatePublicationsTable();
    }

    private void setListeners() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        indexBut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onIndexBut();
            }
        });

        uploadBibFileBut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onUploadFileBut();
            }
        });
    }

    //get citation index and show it in messgDialog
    private void onIndexBut() {
        int index = 10;//TODO: calculate citation index here
        JOptionPane.showMessageDialog(null,
                "Your citation index: " + Integer.toString(index),
                "Citation index",
                JOptionPane.PLAIN_MESSAGE);
    }

    //upload bib file and update publications table on success
    private void onUploadFileBut() {
        String path = FormsManager.getFilePath(this);
        try {
            bibtex.parseFile(path);
            updatePublicationsTable();
            bibtex.updateDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePublicationsTable() {
        ArrayList<Publication> publications = bibtex.getAuthorsPublications(user.name);
        Object colNames[] = {"Title", "Year"};
        Object data[][] = new Object[publications.size()][2];
        for (int i = 0; i < publications.size(); ++i){
            data[i] = publications.get(i).toArray();
        }
        publicationsTable.setModel(new DefaultTableModel(data, colNames));
        publicationsTable.setFillsViewportHeight(true);
    }

    public static void main(String[] args) {
        User user = new User();
        user.name = "Jass, Hugh";
        new AuthorForm(user).setVisible(true);
    }
}
