import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import BibTex.Bibtex;
import BibTex.Publication;
import FormsManager.FormsManager;
import User.*;
import org.jbibtex.BibTeXEntry;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Ann on 24.05.2017.
 */

public class OperatorForm extends JFrame{

    private final int WIDTH = 1000;
    private final int HEIGHT = 500;

    private User user;
    private Bibtex bibtex = Bibtex.getInstance();
    ArrayList<Publication> publications;

    private JPanel operatorPanel;
    private JLabel operatorLabel;
    private JTabbedPane tabbedPane1;
    private JPanel authorsPanel;
    private JPanel publicationsPanel;
    private JTable publicationsTable;
    private JTable authorsTable;
    private JButton uploadBibFileBut;
    private JButton showDuplicatesBut;
    private JButton deletePublicationBut;
    private JButton showPublicationsBut;
    private JButton addAuthorBut;
    private JButton deleteAuthorBut;

    public OperatorForm(User operator){
        super();
        this.user = operator;
        setSize(WIDTH, HEIGHT);

        setGUIComponents();
        setListeners();
    }

    private void setGUIComponents(){
        setTitle("bib-system/operator");

        setContentPane(operatorPanel);
        operatorPanel.setVisible(true);

        operatorLabel.setText(user.name);
        this.updatePublicationsTable();
    }

    private void setListeners(){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        uploadBibFileBut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onUploadFileBut();
            }
        });

        deletePublicationBut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onDeletePublicationBut();
            }
        });
    }

    private void onUploadFileBut(){
        String path = FormsManager.getFilePath(this);
        try {
            bibtex.parseFile(path);
            updatePublicationsTable();
            bibtex.updateDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePublicationsTable(){
        publications = bibtex.getAllPublications();
        Object colNames[] = {"Title", "Year"};
        Object data[][] = new Object[publications.size()][2];
        for (int i = 0; i < publications.size(); ++i){
            data[i] = publications.get(i).toArray();
        }
        publicationsTable.setModel(new DefaultTableModel(data, colNames));
        publicationsTable.setFillsViewportHeight(true);
    }

    private void onDeletePublicationBut(){
        Object[] data = getSelectedData();
        deleteSelectedData(data);
        updatePublicationsTable();
    }

    private Publication[] getSelectedData(){
        int[] selectedRows = publicationsTable.getSelectedRows();
        Publication data[] = new Publication[selectedRows.length];
        for(int i = 0; i < selectedRows.length; i++) {
            data[i] = publications.get(selectedRows[i]);
        }
        return data;
    }

    private void deleteSelectedData(Object[] data){
        bibtex.deleteEntries((Publication[]) data);
    }

    public static void main(String[] args) {
        User user = new User();
        user.name = "Jass, Hugh";
        new OperatorForm(user).setVisible(true);
    }
}
