package DBManager;

import DBManager.Tables.*;
import User.*;

import java.sql.*;

/**
 * Created by Ann on 22.05.2017.
 */
public class DbAdapter {
    private static final String DB_NAME = "./assets/bib_system.db";
    private static DbOpenHelper dbOpenHelper = null;

    public DbAdapter() {
        dbOpenHelper = new DbOpenHelper(DB_NAME);
    }

    public void open() throws Exception {
        if (!dbOpenHelper.openDb())
            throw new Exception(dbOpenHelper.getLastMesg());
    }

    public void close() throws Exception {
        if (!dbOpenHelper.closeDb())
            throw new Exception(dbOpenHelper.getLastMesg());
    }

    //return user(id, role, name)
    public User getUserByPasswordAndLogin(String pass, String login) {
        User user = new User();
        try {
            Statement st = dbOpenHelper.getConnection().createStatement();

            ResultSet res = st.executeQuery(
                    "SELECT " + Users.idColName + "," + Users.roleColName + "," + Users.nameColName +
                            " FROM " + Users.tableName +
                            " WHERE LOGIN='" + login + "' AND PASSWORD='" + pass + "';");

            if (!res.isAfterLast()) {
                user.id = res.getInt(1);
                user.role = Role.values()[res.getInt(2)];
                user.name = res.getString(3);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    /*public static void main(String[] args) {
        DbAdapter adapter = new DbAdapter();
        try {
            adapter.open();
            System.out.println(adapter.getUserByPasswordAndLogin("54321", "pashaT@gmail.com").name);
            adapter.close();
        } catch (Exception e) {
           System.out.println(e.getMessage());
        }
    }*/
}
