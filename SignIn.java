import javax.swing.*;
import java.awt.event.*;

import DBManager.DbAdapter;
import User.*;

public class SignIn extends JDialog {
    private JPanel contentPane;
    private JButton buttonSignIn;
    private JButton buttonCancel;
    private JPasswordField passwordField;
    private JTextField textFieldLogin;

    public SignIn() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonSignIn);

        buttonSignIn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public static void main(String[] args) {
        SignIn dialog = new SignIn();
        dialog.setTitle("bib-system/sign_in");
        dialog.pack();
        dialog.setVisible(true);
    }

    private void onOK() {
        DbAdapter dbAdapter = new DbAdapter();
        try {
            dbAdapter.open();

            String pass = new String(passwordField.getPassword());
            String login = textFieldLogin.getText();

            if (pass != null && login != null)
            {
                User user = dbAdapter.getUserByPasswordAndLogin(pass, login);
                checkUserAndStartNewFrame(user);

            } else
                JOptionPane.showMessageDialog(null, "Wrong login or password!\nCheck input.");

            dbAdapter.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void checkUserAndStartNewFrame(User user){
        if(user.id != -1)
        {
            if(user.role == Role.AUTHOR)
                startAuthorFrame(user);
            else
                startOperatorFrame(user);
            dispose();
        }
        else
            JOptionPane.showMessageDialog(null, "Wrong login or password!\nCheck input.");
    }

    private void startAuthorFrame(User author){
        new AuthorForm(author).setVisible(true);
    }

    private void startOperatorFrame(User operator){
        new OperatorForm(operator).setVisible(true);
    }

    private void onCancel() {
        dispose();
    }
}