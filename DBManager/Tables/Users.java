package DBManager.Tables;

import User.User;
/**
 * Created by Ann on 22.05.2017.
 */
public class Users {
    public static final String tableName = "User";

    public static final String idColName = "ID";
    public static final String nameColName = "NAME";
    public static final String loginColName = "LOGIN";
    public static final String passwordColName = "PASSWORD";
    public static final String roleColName = "ROLE";

    public static final String[] colNames = { idColName, nameColName, loginColName, passwordColName, roleColName };

    public String createFullInsertSql(User user){
        return "INSERT INTO " + this.tableName + " (" +
                this.nameColName +", " +
                this.loginColName + ", " +
                this.passwordColName + ", " +
                this.roleColName + ")  VALUES (" +
                user.name + ", " +
                user.login + ", " +
                user.password + ", " +
                user.role + ");";
    }
}
