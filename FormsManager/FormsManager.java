package FormsManager;

import javax.swing.*;

/**
 * Created by Ann on 24.05.2017.
 */
public class FormsManager {
    //show it if someth isn`t correct
    public static void showErrMessage(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    //show if you want to inform about someth
    public static void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.INFORMATION_MESSAGE);
    }

    //gets file path ofselected file
    public static String getFilePath(JFrame frame){;
        JFileChooser file = new javax.swing.JFileChooser();
        file.setMultiSelectionEnabled(false);

        String path = "";
        if (file.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION)
            path = file.getSelectedFile().getPath();

        return path;
    }

    //String path = JOptionPane.showInputDialog(null, "Input bib-file path", "Upload", JOptionPane.PLAIN_MESSAGE);
}
