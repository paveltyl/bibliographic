package BibTex;

import org.jbibtex.BibTeXEntry;

/**
 * Created by pashatyl on 24/5/17.
 */
public class Publication {
    private static int _id = 0;
    public int id;
    public BibTeXEntry entry;

    public Publication(BibTeXEntry entry) {
        this.entry = entry;
        id = ++_id;
    }

    public Object[] toArray() {
        return new Object[]{
                entry.getField(BibTeXEntry.KEY_TITLE).toUserString(),
                entry.getField(BibTeXEntry.KEY_YEAR).toUserString(),
                id
        };
    }
}
