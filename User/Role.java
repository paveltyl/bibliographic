package User;

/**
 * Created by Ann on 22.05.2017.
 */
public enum Role {
    OPERATOR,
    AUTHOR
}
