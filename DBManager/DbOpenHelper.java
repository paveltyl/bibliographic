package DBManager;

import DBManager.Tables.*;

import java.sql.*;
import java.util.logging.*;

/**
 * Created by Ann on 22.05.2017.
 */
public class DbOpenHelper {
    private String DB_NAME = null;
    private Connection connection = null;
    private String mesg = "";

    public DbOpenHelper(String dbName) {
        DB_NAME = dbName;
    }

    public boolean openDb() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);

            mesg = "opened database " + DB_NAME + " successfully";

            return true;
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            mesg = "opene database " + DB_NAME + " failed";
            return false;
        }
    }

    public boolean closeDb() {
        try {
            connection.close();

            mesg = "database closed succesfully";

            return true;
        } catch (SQLException ex) {
            System.err.println(DbOpenHelper.class.getName());
            mesg = "closed failed";
            return false;
        }
    }

    public boolean createTable(String sql) {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            stmt.execute(sql);
            stmt.close();

            mesg = "table created successfully";

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DbOpenHelper.class.getName()).log(Level.SEVERE, null, ex);
            mesg = "table creation failed with sql = " + sql;
            return false;
        }
    }

    public boolean createTablesIfNotExists() {
       /* if(createTable(someTable.sqlCreate))
            if(createTable(sometable.sqlCreate))
                if(createTable(someTable.sqlCreate)){
                    this.mesg = "tables created/opened successfully";
                    return true;
                }*/

        this.mesg = "creating tables failed";

        return false;
    }

    public String getLastMesg() {
        return this.mesg;
    }

    public Connection getConnection() {
        return connection;
    }
}
